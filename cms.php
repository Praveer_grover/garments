<?php
	ob_start();
	session_start();
	include 'init.php';
	$work = new work();
	//parse url for correct page requested
	$cms_control=new cms_controller();
	$db_model=$cms_control->getModel();
	//After Login, checking if user is logged in
	if(isset($_SESSION['admin'])){
		//List all Products
		if (isset($_GET['products'])){						
		
		}
		//Search
		elseif (isset($_GET['search'])) {
			 //working ,done 
		    //Search 
			$cms_control->getView('search');
		}
		elseif (isset($_GET['views'])) {			
			//View all products.	
			$products = $cms_control->get_all_products();
			
			foreach ($products as $res){
						$item_id = $res['item_id'];
						$type = $res['type'];
						$photo = $res['photo'];
						$description = $res['description'];											
						echo "<div class='product-tile'>";
							echo "<img src='$photo'>";
							echo "<div>$description</div>";
							echo "<div class='buy-btn' onclick='edit($item_id)'>Click Here to Edit</div>";
						echo "</div>";					
					}
		}
			elseif (isset($_GET['view'])&&isset($_GET['item_id'])) {
			# code...
				//Working done !!

			//to view a single item for the admin to see the individual item details in orders
			$criteria =  "item_id";
			$val = $_GET['item_id'];
			$colmns= array('name','photo','orig_price');
			$where = array($criteria =>"$val" );
			$result=$db_model->select('item',$colmns,$where);
			//$output='<table><tr></tr>';
			foreach ($result as  $row) {
			//	$output.='';
				#code...
				
				foreach ($row as $key => $value) {
				//Check HEre For The Results Returned By The Array And Echo The Searched Result With Details And The Image 
				//Of It.
						if($key=='name')
						{
						echo "<h1>$value</h1>";	
						}
						elseif ($key=='photo') {
						echo "<img src='$value'/>";
						}
						elseif ($key=='orig_price') {
							# code...
						echo "<h4>Price :$value</h4>";
						}

					}
			//	$output.='';
			}
		
			//$output.='</table>';
			//echo $output;
			
		}

		//Product Details , edit , delete option
		elseif (isset($_GET['item_id'])&&!isset($_GET['view'])) {
			//get_single_view

			//working done !!
			//this is for editing ,deleting a product 
			$item_id=$work->sanitize($_GET['item_id']);
			$prod_cntrol=new product_controller();
			$prod_cntrol->get_single_view_cms($item_id);
		}		
		//Item Adding Option
		elseif(isset($_GET['upload'])){	
			//Working Done !!

			$cms_control->getView('upload');
		}

		elseif($_SERVER['REQUEST_METHOD']=='POST'&&isset($_POST['trans_id']))
		{
			//Not tested !!

			//This post is needed to make a transaction id complete or incomplete 
			//This request is coming from cms_controller.php 
			$san_trans_id=$work->sanitize($_POST['trans_id']);
			$col_values= array('pending'=>'1');
			$where= array('tid' => $san_trans_id );
			$db_model->update('transaction',$col_values,$where);
			echo '<h2>Changes Successfully Made</h2>';

		}		

			//Here it means that the post request is to upload new items after the session has been set and the user stands logged in 
		elseif($_SERVER['REQUEST_METHOD']=='POST' && isset($_POST['title']))
		{
			//Working Done 



			//when a new item is uploaded then this function is called.
			//$_POST['sex']==1 means female otherwise male.			
			$tmp_name = $_FILES['filein']['tmp_name'];	
			$name = $_FILES['filein']['name'];
			$link = "resources/products/$name";
			move_uploaded_file($tmp_name, $link);
			$san_title=$work->sanitize( $_POST['title']);
			$san_sex=$work->sanitize($_POST['sex'] );
			$san_desc=$work->sanitize($_POST['desc']);
			$san_price=$work->sanitize($_POST['price']);
			$san_discp=$work->sanitize($_POST['dprice']);			
			$values= array('name' =>$san_title,'photo'=>$link,'gender'=>$san_sex,'description'=>$san_desc,'orig_price'=>$san_price ,'disc_price'=>$san_discp);
			//Some Values Are Left Will Be Decided Later What To Do With Them
			$db_model->insert('item',$values);
			//Move to other pages or clear the textboxes after the post has been performed			
			echo "A new Item Has been added successfully";
		}
		//This is for the post request for some search(by name or by id)  in the cms page after the user stands logged in 
		


		elseif ($_SERVER['REQUEST_METHOD']=='POST' && $work->sanitize($_POST['submit'])=='Search') 
		{

			//Working ,done 


			//Cheek Here what criteria is used for searching 
			$criteria='';
				//Echo this variable to output the complete table
			$output='<table><tr> <th></th> </tr>';

			$san_type=$work->sanitize($_POST['type']);
			$san_search=$work->sanitize( $_POST['search']);

			if($san_type=='id')
			{
				$criteria='item_id';
			}
			elseif ($san_type=='name') {
				$criteria='name';
			}
			#       &#8377
			$colmns= array('name','photo','orig_price');
			$where = array($criteria =>$san_search);
			$result=$db_model->select('item',$colmns,$where);
			foreach ($result as  $row) {
				$output.='';
				#code...
				
				foreach ($row as $key => $value) {
				//Check HEre For The Results Returned By The Array And Echo The Searched Result With Details And The Image 
				//Of It.
						if($key=='name')
						{
							$output.="<tr><td>$value</td></tr>";
						}
						if($key=='photo')
						{
							$output.="<tr><td><img src='$value'></td></tr>";
						}
						if($key=='orig_price')
						{
							$output.="<tr><td>$value</td></tr>";
						}




					}

				$output.='';
			}
		


			$output.='</table>';
			echo $output;
			
		}
		elseif (isset($_GET['orders'])) {
			# code...
			$cms_control->getCompleteOrder(1);
		}

		//Main Page After Login in CMS
		else{
			//Praveer - Functionality to show by this condition : List all products , search ,individual product edit,
			//To complete an order,
			//This may be left blank for now will be decided later which will be the default page
			//Decided :It will show all the orders completed or incomplete with each and every details.			
			$cms_control->get_main_view();			
			
		}
	}
	//If not logged in then Login Controller
	else{
		//If there is any post request,it may be for login,or uploading information
		if($_SERVER['REQUEST_METHOD']=='POST')
		{
			//Request For Login 			
			//Check Here in the database if the user exist to validate or not
			$uname = $work->sanitize($_POST['name']);
			$password_check = md5($work->sanitize($_POST['pswd']));
			$flag = 0;
			$colmns=array("username","password");
			$where=array("username"=>$uname,"password"=>$password_check);
			$result=$db_model->select('admin',$colmns,$where);			
			foreach ($result as  $row) {
				#code...
				$password = $row['password'];				
				if($password_check==$password){
					//Make a session and reload this page so that the if part executes now with the session variable set 						
					$_SESSION['admin']=1;
					$flag = 1;							
					header('Location:cms.php');									
				}						
			}
			if(!$flag){			
				//Go To The Login Page Again Because Of The Wrong Credentials 
				$cms_control->getView('login_fault');						
			}
		}
		else
		{
			//Just display the login page			
			$cms_control->getView('login');			
		}
	}
?>
<script type="text/javascript">
  function edit(item){
  	url = document.URL;
  	url = url.split('?');
  	url = url[0];
  	url += "?item_id="+item;
  	window.location.assign(url);
  }
</script>