<html>
<head>
	<title>Upload New Items</title>
<!--<script type="text/javascript" src="views/jquery.js"></script>-->
<link type='text/css' rel='stylesheet' href='views/css/upload.css'/>
<link href='http://fonts.googleapis.com/css?family=Exo+2:300' rel='stylesheet' type='text/css'/>
<script type="text/javascript" href="views/js/upload.js"></script>


</head>
<body>
	<?php
	if(!isset($_SESSION['admin']))
	{
		header('location:../cms.php');
	}
	?>
<div class='form'>
	<h1>Upload Items</h1>
	<form method='post' action='cms.php' enctype="multipart/form-data">
		<table>
			<tr>
			<td>Select The Image Of The Item</td>
			<td><input type='file' name='filein' id='image' class='upload'/></td>
			</tr>
			<tr> 
			<td>Enter Name Of The Item</td>
			<td><input type='text' name='title' class='upload'/></td>
			</tr>
			<tr> 
			<td>Enter Description Of The Item</td>
			<td><input type='text' name='desc' class='upload'/></td>
			</tr>
			<tr> 
			<td>Enter Price Of The Item(In Rs.)</td>
			<td><input type='text' name='price' class='upload'/></td>
			</tr>
			<tr> 
			<td>For Whom</td>
			<td><input type="radio" name="sex" value="0">Men&nbsp;&nbsp;<input type="radio" name="sex" value="1" checked>Women</td>
			</tr>
			<tr>
			<td>Disounted Price(If Applicable)</td>
			<td><input type='text' name='dprice' /></td>
			</tr>			
			<tr>
			<td colspan='2' align='center'><input type='submit' id ="submit" name="submit" value='upload' onClick='validate()'></td>
			</tr>			
			</table>
	</form>
</div>
	<script type="text/javascript">
		
	</script>
</body>
</html>