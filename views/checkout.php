<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="css/main_page.css"/>
<script type='text/javascript' href='js/upload.js'></script>

<body bgcolor='#ecf0f1'>
	<div class='checkout-head'><header> <h1> Checkout Your Cart</h1></header></div>
<?php	
	include '../init.php';
	$work_model=new work();
	$db=new database();
	
if(isset($_COOKIE['items']))
{

	$items=$_COOKIE['items'];
	$items = urldecode($items);	
	$item_explode=explode(',',$items);	
	if(isset($_POST['checkout']))
	{
		$amount=$work_model->get_amount($_COOKIE['items']);		
		$san_name=$work_model->sanitize($_POST['name']);
		$san_address=$work_model->sanitize($_POST['address']);
		$san_phone=$work_model->sanitize($_POST['phone']);
		$san_email=$work_model->sanitize($_POST['email']);
		$value_trans=array('items'=>$_COOKIE['items'],'name'=>$san_name,'email'=>$san_email,'phone'=>$san_phone,'address'=>$san_address,'amount'=>$amount );
		
		//Praveer return t_id from the previous statement so that it can be used in the insertion of the order table
		
		$db->insert('transaction',$value_trans);
		
		//clear the cookies		
		setcookie('items','',time()+10,'/');
	}

	elseif (isset($_POST['delete_item'])) {

		
		$san_delete_item=$work_model->sanitize($_POST['delete_item']);
		//echo $san_delete_item;
		$new_cookie='';
		foreach ($item_explode as $value) {
			
			if($san_delete_item!=$value && $value!=end($item_explode))
			{
				//echo $value;
				$new_cookie.=$value.',';
			}
			elseif ($san_delete_item!=$value) {
				$new_cookie.=$value;
			}
			//echo $new_cookie;
		//Again reload the page to hide the deleted items.
		
	
		header("Location:checkout.php");
//Set the new cookie with the content of the new cookie
		


		}
	
			setcookie('items',$new_cookie,time()+3600,'/');



		# code...
	}
	else
	{		
		//Show all the items with their total price and details of each product		
		$output =  "<div class='checkout-table'>";
		$output .= "<table><tr><th></th><th>Item Name</th><th>Price</th></tr>";
		//Praveer Apply Some CSS to the table

		foreach ($item_explode as $value) {
				# Make a table here showing the image with some details of all the items in 
					$result1=$db->select('item',array('photo','name','orig_price','item_id' ),array('item_id' => $value ));					
					$row=$result1[0];
							//$index++;
							$name = $row['name'];
							$price = $row['orig_price'];
							//$quants = $_COOKIE['quantities'];
							//$quantity = $_COOKIE['']
							//$quantity = $quants[$index];
							$photo = '../'.$row['photo'];
						
		$output .= "<tr><td><img src='$photo'></td><td>$name</td>"."<td>$price</td>"."<td><form   action='checkout.php' method='post'><input type='hidden' name='delete_item' value='".$row['item_id']."'><div class='delete'><input type='Submit' value='Delete'></div></form></td></tr>";
			

			}
			$output.='</table></div>';
			echo $output;
	}
}

?>
<div class='checkout-main-body' id='myformdiv'>
	<form method="post">
		<table>
			<tr>				
				<td><input type='text' name='name' placeholder="Your Name"/></td>
			</tr>
			<tr>				
				<td><input type='text' name='address' placeholder="Your Address"></td>
			</tr>
			<tr>				
				<td><input type='text' name='phone' placeholder="Your Phone Number"/></td>

			</tr>
			<tr>				
				<td><input type='email' name='email' placeholder='Your Email Address'/></td>
			</tr>
			<tr>
				<td colspan=2><input type='submit' name='checkout' value='Place Order'></td>
			</tr>
		</table>	
	</form>
</div>

</body>
