<html>
<head>
	<title>Edit Item</title>
	<link rel="stylesheet" type="text/css" href="views/css/product.css"/>
	<link href='http://fonts.googleapis.com/css?family=Exo+2:300' rel='stylesheet' type='text/css'/>
	<script type='text/javascript' href='js/upload.js'></script>
	<script type="text/javascript" src="views/jquery.js"></script>
	<link rel="stylesheet" type="text/css" href="imagearea/css/imgareaselect-default.css" />
  	<script type="text/javascript" src="imagearea/scripts/jquery.min.js"></script>
  	<script type="text/javascript" src="imagearea/scripts/jquery.imgareaselect.pack.js"></script>

</head>
<body>


<?php
	
	//Ask praveer what is this ?
	//$details = $this->detail_prod($item_id);
	
	if($_SERVER['REQUEST_METHOD']=='POST'&&isset($_POST['delete']))
	{
		include '../init.php';
		$db=new database();
		$work = new work();
		
		//Delete The Requested Item From The Db. 
		$san_id=$work->sanitize($_POST['id']);
			$where= array('item_id' => $san_id );
			$db->delete('item',$where);

			echo "<h2 id='status'>Successfully Deleted Item</h2>";

	}
	elseif($_SERVER['REQUEST_METHOD']=='POST'){
		//Make Changes to the row of item_id	
		include '../init.php';			
		$db=new database();
		$work = new work();	
		$san_id=$work->sanitize($_POST['id']);
		$san_title=$work->sanitize( $_POST['title']);
		$san_sex=$work->sanitize($_POST['sex']);

		$san_desc=$work->sanitize($_POST['desc']);
		$san_price=$work->sanitize($_POST['price']);
		$san_discp=$work->sanitize($_POST['dprice']);
		
		
		$where= array('item_id' => $san_id);
		$values= array('name' =>$san_title,'gender'=>$san_sex,'description'=>$san_desc,'orig_price'=>$san_price ,'disc_price'=>$san_discp);			
		
		$db->update('item',$values,$where);			
		echo "<h2 id='status'>Changes Successfully Done</h2>";
		//Redirect to the admin home page again
	}
	else{


//include 'init.php';			
		$db=new database();
	$work = new work();	
	//Ye sa

		//Display the form details over here for editing	
		if (isset($_GET['item_id'])) {		
		$san_id=$work->sanitize($_GET['item_id']);
		$where= array('item_id' => $san_id);
		$colmns= array('name','photo','description','orig_price','disc_price','gender');

		$res=$db->select('item',$colmns,$where);
		$form='';
		$form.="<div><h1>Edit Items</h1><form id='form1' method='POST' action='/garments/views/product.php' class='form' ><input type='hidden' value='$san_id' name='id'/><table>";
						foreach ($res as  $row) {
					#code...
					foreach ($row as $key => $value) {
						if($key=='photo')
						{
							$form.="<tr><td>Iamge</td><td><img src='$value' alt='Sorry Image Cannot Be Loaded' ></td></tr>";
						}
						elseif($key=='name')
						{
							$form.="<tr><td>Name</td><td><input  name='title' type='text' value='$value'></td></tr>";
						
						}
						elseif($key=='description')
						{
							$form.="<tr><td>Description</td><td><input name='desc' type='text' value='$value'></td></tr>";
						
						}
						elseif($key=='orig_price')
						{
							$form.="<tr><td>Original Price</td><td><input name='price' type='text' value='$value'></td></tr>";
						
						}
						elseif($key=='disc_price')
						{
							$form.="<tr><td>Discounted Price</td><td><input name='dprice' type='text' value='$value'></td></tr>";
						
						}
						elseif($key=='gender')
						{
							if($value=='1')
							{
								//Female
								$form.='<tr><td>For Whom </td><td><input type="radio" name="sex" value="0">Men&nbsp;&nbsp;<input type="radio" name="sex" value="1" checked>Women</td></tr>';
							}
							else{
								//Male 
								$form.='<tr><td>For Whom </td><td><input type="radio" name="sex" checked value="0">Men&nbsp;&nbsp;<input type="radio" name="sex" value="1" >Women</td></tr>';
							}

						}


					}

					}	

			$form.="<tr><td><input id='submit'  type='submit' value='Save' /></td><td><input id='delete' type='submit' name='delete' value='Delete' onClick='confirmdelete()' ></td></tr>";
			$form.='</table></form></div>';
			echo "$form";
		
			}
			else
			{
				echo 'hello';
				//redirect to some other page because there is not a valid query string 
			}

		
	}
?>
<script type="text/javascript">
		$(document).ready(function () {
    					$('#image').imgAreaSelect({
        				handles: true,
        				onSelectEnd: someFunction,
        				aspectRatio:4:3
    				});
			});
</script>
</body>
</html>