<?php
	
	@session_start();
	if(isset($_SESSION['admin'])){		
?>	
	<header>
		<h1>Content Managment System</h1>
		<span>Logout</span>
	</header>
	<div class="cms-tiles">
		<div onclick="window.location.assign(document.URL+'?orders')"><img src="resources/icons/table.png">Orders</div>
		<div onclick="window.location.assign(document.URL+'?views')"><img src="resources/icons/product.png">Products</div>
		<div onclick="window.location.assign(document.URL+'?upload')"><img src="resources/icons/plus.png">Add a Product</div>
		<div onclick="window.location.assign(document.URL+'?search')"><img src="resources/icons/search.png">Search a Product</div>
	</div>
<?php 
	}
	else{
		header('Location:cms.php');
	}
?>