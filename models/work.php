<?php
/*
* Work class for general functions like sanitize etc.
*/
	include 'database.php';
	class work extends database{
		function __construct(){
			parent::__construct();
		}
		function __destruct(){
			parent::__destruct();
		}
		function sanitize($entity){
			$entity = strip_tags($entity);
			return $entity;
		}
		//Check If admin is logged in
		function check_online(){

		}

		//Calculate amount from items
		function get_amount($items){
			$items = urldecode($items);
			$items = explode(",", $items);
			$amount = 0;
			foreach($items as $item) {
				$columns = array('orig_price');							
				$where = array('item_id'=>$item);
				$result = $this->select('item',$columns,$where);
				$check = 0;
				foreach ($result as $row) {
					if(is_array($row)){
						$amount += $row['orig_price'];
					}
					elseif(!$check){
						$amount+=$result['orig_price'];
						$check = 1;
					}
				}
			}
			return $amount;
		}
		
	}
?>