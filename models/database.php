<?php
/**
* Database Class
*/	
	class database{
		
		public $link;
		function __construct(){
			$this->link = new mysqli('localhost','root','','garments');//'sql3.freemysqlhosting.net','sql329332','gI7*wV9*','sql329332'
			//$this->link = new mysqli('us-cdbr-azure-east-c.cloudapp.net','b8a60d8f1992b2','e9af584c','garments');//'sql3.freemysqlhosting.net','sql329332','gI7*wV9*','sql329332'
	
		}
		function __destruct(){

			$this->link->close();

		}
		/*
		*	Select query on database
		* 	
		*	$rows is an array of things to be selected, where is an array of clause=>value
		* 	output returned is an array of rows fetched from the table
		*	$rows=array("name","price");
		*   $where = array("name"=>"Praveer",""=>"");
		* 	$output = array(array("name"=>"Praveer","order_id"=>1),array("name"=>"Ragh"),array());
		* 	
		*/
		function select($tableName,$rows,$where="",$order_by=""){
			$where_clause = "";
			$where_value = "";
			$where_string = "";
			$output = array();			
			if(count($rows)!=1)
			{
				$row = implode(',', $rows);
			}
			else
			{
				$row = $rows[0];
			}
			if($where=="")
			{
				$where_clause=$where_value=1;
				$where_string = $where_clause.'='.'\''.$where_value.'\'';

			}
			else
			{
				foreach ($where as $clause => $value)
				{

					if(end($where) != $value)
					{
						$where_clause = $clause;
						$where_value = $value;
						$where_string = $where_clause.'='.'\''.$where_value.'\''.'and';
					}
					else
					{
						$where_string = $clause.'='.'\''.$value.'\'';
						
					}
				}
			}
			// WHERE clauses append a=b and c=d
			//ORDER BY '$order_by'			
			$result = $this->link->query("SELECT $row FROM $tableName WHERE $where_string");
			//Fpr Debug
			//echo "SELECT $row FROM $tableName WHERE $where_string";
			//echo $this->link->error;
			while($row1 = $result->fetch_array(MYSQLI_ASSOC))
			{
				array_push($output, $row1);
			}
			$result->free();
			return $output;
		}
		/*************************
		* update query parameters tablemane , 
		* column values to be updated and pass as an array a => value , b => value
		* similarly for where
		**************************/
		function update($tablename,$col_values,$where){
			//Praveer make the update function.
			$set_string = "";
			$where_string  = "";
			if($where=="")
			{
				$where_clause=$where_value=1;
				$where_string = $where_clause.'='.'\''.$where_value.'\'';
			}
			else
			{
				foreach ($where as $clause => $value)
				{
					if(end($where) != $value)
					{
						$where_clause = $clause;
						$where_value = $value;
						$where_string .= $clause.'='.'\''.$value.'\''.'and';
					}
					else
					{
						$where_string .= $clause.'='.'\''.$value.'\'';
					}
				}
			}
			foreach ($col_values as $column => $value)
			{
				if(end($col_values) != $value){
					$set_string .= $column.'='.'\''.$value.'\''.',';
				}
				else{
					$set_string .= $column.'='.'\''.$value.'\'';
				}
			}
			//Set string a=b , c=d
			//echo "UPDATE $tablename SET $set_string WHERE $where_string";
			$this->link->query("UPDATE $tablename SET $set_string WHERE $where_string");
			//echo $this->link->error;
		}

		/*
		*	Insert query
		*	Values will be array in format
		* 	array(column1 => value1,column2 => value2);
		* $tablename = "item";
		* $values = array("name"=>"Praveer","price"=>"1");
		* $obj->insert($tablename,$values)
		*/
		function insert($tableName,$values){
			$value = "";
			$column = "";
			foreach ($values as $col => $val)
			{
				if(end($values) != $val)
				{
					$value.='\''.$val.'\''.",";
					$column.=$col.",";
				}
				else
				{
					$value.='\''.$val.'\'';
					$column.=$col;
				}
			}
			//echo $value."<br>".$column;
			$this->link->query("INSERT INTO $tableName($column) VALUES($value)");
			//echo $this->link->error;
		}
		/*
		* 	Delete query
		*	arguments : tableName, where key value pair array
		*/
		function delete($tableName,$where){
			$where_string = "";
			if($where=="")
			{
				$where_clause=$where_value=1;
			}
			else
			{
				foreach ($where as $clause => $value)
				{
					if(end($where) != $value)
					{
						$where_clause = $clause;
						$where_value = $value;
						$where_string .= $clause.'='.'\''.$value.'\''.'and';
					}
					else
					{
						$where_string .= $clause.'='.'\''.$value.'\'';
					}
				}
			}
			$this->link->query("DELETE FROM $tableName WHERE $where_string");
		}
	}
?>