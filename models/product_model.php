<?php
/*
*	Model Of Products : will load Products
*/
	class product_model extends work{
		function __construct(){
			parent::__construct();
		}
		function __destruct(){
			parent::__destruct();
		}
		function get_products($type){
			//Select products from db with gender $type
			$where = array('gender' => $type);
			$products = $this->select('item',array('item_id','name','type','photo','description','orig_price','disc_price'),$where);
			return $products;
		}		
	}
?>

