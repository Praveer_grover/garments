<?php
/*
*	Login Controller
*	To Load Login model , view
*/
	class cms_controller{
		protected $db_model;
		function __construct(){
			$this->db_model = new database();
		}
		function __destruct(){

		}
		function getModel(){
			//Instantiate a new database model here 
				
				return $this->db_model;			
		}
		function getView($var){
			if($var=='login'){
				require_once "views/login.html";
			}
			elseif($var=='upload'){
				require_once "views/upload.php";
				//Call Here the Upload Model
			}
			elseif ($var=='search'){
				# code...
				require_once "views/search.php";
			}	
			elseif ($var=='login_fault') {
						# code...
				require_once "views/login.html";
				//Ask praveer if this querystring will work or not
					}		
		}

		function getCompleteOrder($type){
			//require_once "views/order.php";
			?>

			<html>
			<head>
			<link rel='stylesheet' type='text/css' href='views/css/cms_controller.css'/>

			</head>

			<body>
			<?php
			$form='';
			$form.="<table><tr><th>Transaction Id</th><th>Items</th><th>Name</th><th>Phone</th><th>Address</th><th>Email</th><th>Amount</th><th>Order Recieved On</th><th>Pending</th></tr>";
			$rows=array("tid","items","name","phone","address","email","amount","timestamp","pending");
			$result=$this->db_model->select('transaction',$rows);			
			foreach ($result as  $row) {
				#code...
				$form.='<tr>';
				foreach ($row as $key => $value) {
				//Check HEre For The Results Returned By The Array And Echo The Searched Result With Details And The Image 
				//Of It.
						$tid = $row['tid'];
						if($key=='pending'&&$value=='0'){

							$form.="<td><form method='POST' action='cms.php'>
							<input type='hidden' name='trans_id' value=$tid/>
							<input type='submit' name='order_comp' value='Complete'/></form></td>";
						}

						elseif ($key=='pending'&&$value=='1') {
							
							//the Case where the incomplete button is to be hidden or disabled.
							$form.="<td>Order Completed</td>";
						}
						elseif ($key='items') {
							$form.='<td>';

							$array1=explode(',',$value);
							
							foreach ($array1 as $value2) {
								# code...									
								$form .="<a href='cms.php?item_id=".$value2."&view'>$value2</a><br/>";

							}
							$form.='</td>';
							# code...
						}
						else{

							$form.="<td>$value</td>";
						}
				
				}

				$form.='</tr>';
			}
			
			$form.='</table>';

			echo $form;
				# code..
			?>

			</body>
			</html>
			<?php
		}
		function get_main_view()
		{
			//include 'views/header.html';
			include('views/cms-main.php');
			//include 'views/footer.html';
		}
		function get_all_products(){
			$where = array('1' => '1');
			$products = $this->db_model->select('item',array('item_id','name','type','photo','description','orig_price','disc_price'),$where);
			return $products;
		}
	}
?>