<?php
/*
*	Products Controller
*	To Load Products model , view
*/	
	class product_controller extends work{
		
		function __construct(){
			
		}
		function __destruct(){

		}
		function getModel($gender,$item_id=""){
			//If all products details are to be shown
			if($item_id==""){
				$model = new product_model();
				$products = $model->get_products($gender);
				$this->getView($gender,$products);
			}
			//If detail of a product is to be shown
			else{			
				$this->get_single_view($item_id);
			}
		}
		function getView($gender,$products){
			require_once 'views/header.html';
			//Processing on products and echo out each product with a class
			echo "<div class='products'>";
			$pb = new database();
			switch ($gender){
				case '1':
					// Female
					$rows = array('item_id','type','photo','description','orig_price','disc_price','points');
					$where = array('gender'=>'1');
					$result = $pb->select('item',$rows,$where);
					foreach ($result as $res){
						$item_id = $res['item_id'];
						$type = $res['type'];
						$photo = $res['photo'];
						$description = $res['description'];
						$orig_price = $res['orig_price'];
						$disc_price = $res['disc_price'];
						$points = $res['points'];
						echo "<div class='product-tile'>";
							echo "<img src='$photo'>";
							echo "<div>$description</div>";
							echo "<div class='buy-btn' onclick='add_to_cart($item_id)'>Click Here to Buy</div>";
						echo "</div>";
						echo "<div class='points'>$points</div>";
					}
					echo "<script type='text/javascript'>
							function preview(img, selection) {								
    							var scaleX = 300 / (selection.width || 1);
	    						var scaleY = 238 / (selection.height || 1);  	    					
    							$('.image + div > img').css({
        							width: Math.round(scaleX * 400) + 'px',
        							height: Math.round(scaleY * 300) + 'px',
        							marginLeft: '-' + Math.round(scaleX * selection.x1) + 'px',
        							marginTop: '-' + Math.round(scaleY * selection.y1) + 'px'
    								});																
							}							
							img = document.getElementsByClassName('image');
							points = document.getElementsByClassName('points');							
							length = img.length;
							for(i=0;i<length;i++){
								value = points[i].innerHTML;								
								point = value.split(',');
								selection = {width:'300px',height:'238px',x1:point[0],y1:point[1],x2:point[2],y2:point[3]}
								vals = '$photo';
								$('<div><img src='+vals+'><div>')
        							.css({           
            							position: 'relative',
            							overflow: 'hidden',
            							width:'300px',
            							height:'238px'
        							})
        						.insertAfter($('.image'));        
								preview(img[i],selection);														
        					}
						  </script>";
					break;
				case '0':
					// Male					
					$rows = array('item_id','type','photo','description','orig_price','disc_price');
					$where = array('gender'=>'0');

					$result = $pb->select('item',$rows,$where);										
					foreach ($result as $res){																	
						$item_id = $res['item_id'];
						$type = $res['type'];
						$photo = $res['photo'];
						$description = $res['description'];
						$orig_price = $res['orig_price'];
						$disc_price = $res['disc_price'];
						echo "<div class='product-tile'>";
							echo "<img src=$photo />";
							echo "<div>$description</div>";
							echo "<div class='buy-btn' onclick='add_to_cart($item_id)'>Click Here to Buy</div>";
						echo "</div>";
					}
					echo "<script type='text/javascript'>
							alert('yes');
							function preview(img, selection) {
    							var scaleX = 300 / (selection.width || 1);
	    						var scaleY = 238 / (selection.height || 1);  	
    							$('.image + div > img').css({
        							width: Math.round(scaleX * 400) + 'px',
        							height: Math.round(scaleY * 300) + 'px',
        							marginLeft: '-' + Math.round(scaleX * selection.x1) + 'px',
        							marginTop: '-' + Math.round(scaleY * selection.y1) + 'px'
    								});
							}							
							img = document.getElementsByClassName('image');
							points = document.getElementsByClassName('points');
							points = points.split(';');
							length = img.length;
							for(i=0;i<length;i++){
								point = points[i].split(',');
								selection = {width:300px,height:238px,x1:point[0],y1:point[1],x2:point[2],y2:point[3]}
								preview(img[i],selection);
							}
						  </script>";
					break;
				default:
					# code...
					break;
			}
			echo "</div>";
			//After this call the footer here
			require 'views/footer.html';
		}
		function get_single_view($item_id){
			# code...
			$columns = array('photo','name','orig_price');
			$where = array('item_id'=>$item_id);
			$result = $this->select('item',$columns,$where);
			$row = $result[0];
			$details = $row['photo'].','.$row['name'].','.$row['orig_price'];
			return $details;
		}
		//For cms
		function get_single_view_cms($item_id){			
			include 'views/product.php';	
			//Mainly for cms purpose.		
		}
	}
?>